var app = angular.module('log4420App', []);

app.controller('disciplinesListController', function($scope, $http) {
$scope.name="";
$scope.players = []

$http.get('http://localhost:3000/allPlayers')
	.then(function(responsePlayers){ 
		responsePlayers.data.map(function(player){ 
			$http.get('http://localhost:3000/playerProgression/'+player._id)
				.then(function(progress){ 
					console.log(progress.data);
					$scope.players.push({name: player.playerData.name, progress: progress.data.playerProgression.page, id: player._id}) 
					var button = document.createElement("a");
					var delButton = document.createElement("a");
					var td = document.createElement("td");
					if($scope.players.length%4 == 1)
					{
						$scope.tr = document.createElement("tr");
						document.getElementById("oldPlayers").appendChild($scope.tr); 
					}
					var name = player.playerData.name;
					if(name.length > 15)
						name = name.substring(0,14) + " ... ";
					button.innerHTML = name + " à la page " + progress.data.playerProgression.page;
					button.setAttribute("class", "button");
					button.setAttribute("href", "/resume/"+player._id);
					delButton.innerHTML = "X";
					delButton.setAttribute("href", "/deletePlayer/"+player._id);
					delButton.setAttribute("class", "delButton");
					td.appendChild(button);
					td.appendChild(delButton);
					$scope.tr.appendChild(td);
					
				});
			});
	});
$scope.disciplines = {
	"CAMOUFLAGE":"Camouflage",
	"CHASSE":"Chasse",
	"SIXIEMESENS":"Sixème sens",
	"ORIENTATION":"Orientation",
	"GUERISON":"Guérison",
	"MAITRISEDESARMES":"Maîtrise des armes",
	"BOUCLIEPSYCHIQUE":"Bouclier psychique",
	"PUISSANCEPSYCHIQUE":"Puissance pshychique",
	"COMMUNICATIONANIMALE":"Communication Animale",
	"MAITRISEMATIERE":"Maîtrise pshychique de la matière"
	}

$scope.maitriseArmes = {
	"MAITRISEPOIGNARD":"Maîtrise du poignard",
	"MAITRISELANCE":"Maîtrise de la lance",
	"MAITRISEMASSEDARME":"Maîtrise de la masse d'arme",
	"MAITRISESABRE":"Maîtrise du sabre",
	"MAITRISEMARTEAUDEGUERRE":"Maîtrise du marteau de guerre",
	"MAITRISEEPEE":"Maîtrise de l'épée",
	"MAITRISEHACHE":"Maîtrise de la hâche",
	"MAITRISEEPEE2":"Maîtrise de l'épée",
	"MAITRISEBATON":"Maîtrise du bâton",
	"MAITRISEGLAIVE":"Maîtrise du glaive"
	}

$scope.armes = {
	"POIGNARD":"Poignard",
	"LANCE":"Lance",
	"MASSEDARME":"Masse d'Arme",
	"SABRE":"Sabre",
	"MARTEAUDEGUERRE":"Marteau de Guerre",
	"EPEE":"Épée",
	"HACHE":"Hâche",
	"EPEE":"Épée",
	"BATON":"Bâton",
	"GLAIVE":"Glaive"
	}

$scope.sacADos = {
	"LAMPSUR":"Potion de Lampsur",
	"RATIONS":"Rations Spéciales"	
	}

$scope.objetSpeciaux = {
	"GILETCUIR":"Gilet de Cuir Matelassé"
	}

$scope.disciplineClick = function(id, choice, labelId)
{
	$scope.reduceChoice(choice, labelId);
	if(id == "MAITRISEDESARMES")	
		$scope.activateWeaponMastery();
}

$scope.reduceChoice = function(choice, labelId)
{
	if(labelId == "choixMaitrise")
		choice = choice - $scope.extractDisciplines().length;
	else if(labelId == "choixObjet")
		choice = choice - $scope.extractObjects().length;
	var label = document.getElementById(labelId);
	label.innerHTML = choice + " choix restant";

	if(choice < 0 )
		label.style.color = 'red';
	else
		label.style.color = 'white';
}

$scope.activateWeaponMastery = function()
{
	if(!document.getElementById("maitriseDesArmesList"))
	{
		var maitriseElem = document.getElementById("MAITRISEDESARMES").parentElement;
		var ol = $scope.createMaitriseDesArmesList();
		maitriseElem.appendChild(ol);
	}
	var checkBoxes = document.getElementById("maitriseDesArmesList").getElementsByTagName("input");

	for (var i = 0; i < checkBoxes.length; i++) {
		checkBoxes[i].checked = false;
	};

	if(document.getElementById("MAITRISEDESARMES").checked)
	{
		var index = $scope.randomNumberForElement(0, 9);
		checkBoxes[index].checked = true;
		document.getElementById("MAITRISEDESARMES").setAttribute("name", checkBoxes[index].id);
	}
}

$scope.createMaitriseDesArmesList = function()
{
	var ol = document.createElement("ol");
	ol.setAttribute("id","maitriseDesArmesList");
	for(var key in $scope.maitriseArmes)
	{
		if(key == "MAITRISEEPEE2")
			key = "MAITRISEEPEE"; 
		var li = document.createElement("li");
		var input = document.createElement("input");
		var label = document.createElement("label");
		input.setAttribute("type", "checkBox");
		input.setAttribute("name", key);
		input.setAttribute("id", key);
		input.setAttribute("disabled","disabled");
		label.setAttribute("for", key);
		label.innerHTML = $scope.maitriseArmes[key];
		li.appendChild(input);
		li.appendChild(label);
		ol.appendChild(li);
	}
	return ol;
}

$scope.randomNumberForElement = function(min, max)
{
	return Math.round(Math.random()*(max-min) + min);
}

$scope.extractDisciplines = function()
{
	var disciplines = document.getElementsByClassName('discipline');
	var array = [];
	for (var i = 0; i < disciplines.length; i++) {
		if(disciplines[i].checked)
			array[array.length] = disciplines[i];
	};
	return array;
}

$scope.extractObjects = function()
{
	var objects = document.getElementsByClassName('equipment');

	var array = [];
	for (var i = 0; i < objects.length; i++) {
		if(objects[i].checked)
			array[array.length] = objects[i];
	}
	return array;
}
});

function validateForm(maxDisciplines, maxObjects, name)
{
	var nbDisciplines = extractDisciplines().length;
	var nbObjects = extractObjects().length;
	var name = document.getElementById("nameValue").value;
	if(name != "" && nbDisciplines == maxDisciplines && nbObjects == maxObjects)
		return true;
	document.getElementById("errorMessage").innerHTML = "Vous devez nommez votre personnage et choisir précisément 5 disciplines et 2 objets!";
	return false
}

function extractDisciplines()
{
	var disciplines = document.getElementsByClassName('discipline');
	var array = [];
	for (var i = 0; i < disciplines.length; i++) {
		if(disciplines[i].checked)
			array[array.length] = disciplines[i];
	};
	return array;
}

function extractObjects()
{
	var objects = document.getElementsByClassName('equipment');

	var array = [];
	for (var i = 0; i < objects.length; i++) {
		if(objects[i].checked)
			array[array.length] = objects[i];
	}
	return array;
}