var express = require('express');
var router = express.Router();

var utilities = require('../scripts/jsUtilities');
var dec = require('../scripts/decision.js');

// table des pages ayant une destination aléatoire
var table = { 
  '134': {decision: dec['page_134'], fonction: randomDefault},
  '155': {decision: dec['page_155'], fonction: randomDefault},
  '167': {decision: dec['page_167'], fonction: randomDefault},
  '331': {decision: dec['page_331'], fonction: randomDefault}
};

function randomDefault() {
  return utilities.randomNumberForElement(0,9)
}

// service web pour la destination aléatoire
router.get('/:page/:bonus', function(req, res, next) {
  // On récupère le paramètre de l'URL
  var page = req.params.page;
  var bonus = req.params.bonus;

  // fonction aléatoire selon la page
  var objet = table[page];

    console.log(objet);

  // Vérification de la page
  if (objet == undefined)
  {
    res.json({error: "page invalide"});
    return;
  }

  var nombre = parseInt(objet.fonction());
  nombre += parseInt(bonus);
  var possibilite = objet.decision;
  var destination = -1;


  
  // trouver la destination
  for (i = 0; i < possibilite.length; i++)
  {
    if (possibilite[i].plage.indexOf(nombre) != -1)
      destination = possibilite[i].page;
  }
  
  if (destination == -1)
    console.log('Problème de la plage');

  // création du JSON
  aleatoire = {};
  aleatoire["destination"] = destination;
  res.json(aleatoire);

});

module.exports = router;