﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour {

	[SerializeField]
	private GUIText _scoreIndicator;
	
	[SerializeField]
	GameObject _player1;

	private string score = "score : ";
	private string pts = " pts";
	
	// Use this for initialization
	void Start () {
		_scoreIndicator.text = score + "0 pts";
	}
	
	void OnGUI(){
		_scoreIndicator.text = score + _player1.GetComponent<CarController> ().Score + pts;
	}
}
