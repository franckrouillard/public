using UnityEngine;

[RequireComponent(typeof(CarController))]
public class CarUserControl : MonoBehaviour
{
    private CarController car;  // the car controller we want to use
    

    void Awake ()
    {
        // get the car controller
        car = GetComponent<CarController>();
    }


    void FixedUpdate()
    {
        // pass the input to the car!
#if CROSS_PLATFORM_INPUT
		float h = CrossPlatformInput.GetAxis("Horizontal");
		float v = CrossPlatformInput.GetAxis("Vertical");
		float t = CrossPlatformInput.GetAxis("Tilt");
		bool jump = CrossPlatformInput.GetButton("Jump");
#else
		float h = Input.GetAxis(horizontal);
		float v = Input.GetAxis(vertical);
		bool jump = Input.GetButton("Jump");
#endif
		car.Move(h,v,t);
		if (jump)
		{
			car.Jump();
		}
    }
}
