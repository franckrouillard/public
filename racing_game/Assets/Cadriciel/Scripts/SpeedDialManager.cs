﻿using UnityEngine;
using System.Collections;

// Speed will be from 0 to 260 kph

public class SpeedDialManager : MonoBehaviour {

	[SerializeField]
	private GameObject _speedDial;
	private GUIText _rawSpeedIndicator;

	[SerializeField]
	private Texture _arrowSpeedIndicator;

	[SerializeField]
	GameObject _player1;

	private float _angle = -130f;
	private Vector2 _size = new Vector2(6f,30f);
	private Vector2 _position;

	// Use this for initialization
	void Start () {
		_rawSpeedIndicator = _speedDial.GetComponentInChildren<GUIText> ();
	}

	void OnGUI(){
		//3.6 is to get km per hour
		float player1Speed = Mathf.Round (_player1.rigidbody.velocity.magnitude * 3.6f);
		_rawSpeedIndicator.text = player1Speed + " km/h";

		float angle = Constants.ANGLES_FOR_SPEED.x + Mathf.Lerp (Constants.ANGLES_FOR_SPEED.x,Constants.ANGLES_FOR_SPEED.y, player1Speed / 260f);

		// (Re)calculate pivot position, in case the window size has changed
		_position = new Vector2(Screen.width*0.88f + _speedDial.guiTexture.pixelInset.width/2f,Screen.height*0.95f -  _speedDial.guiTexture.pixelInset.height/2f);

		Matrix4x4 matrixBackup = GUI.matrix;
		GUIUtility.RotateAroundPivot(angle, _position);
		GUI.DrawTexture(new Rect(_position.x, _position.y, _size.x, _size.y), _arrowSpeedIndicator);
		GUI.matrix = matrixBackup;
	}
}
