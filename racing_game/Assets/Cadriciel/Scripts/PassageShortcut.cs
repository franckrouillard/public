﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PassageShortcut : MonoBehaviour {

	[SerializeField]
	private GameObject _carContainer;
	private CheckpointManager checkPointManager;
	private bool entranceToggled = false;
	private Dictionary<CarController,bool> _carEntered = new Dictionary<CarController, bool>();

	/// <summary>
	/// No car has entered yet
	/// </summary>
	void Start () {
		checkPointManager = GameObject.FindGameObjectWithTag (Constants.GAME_MANAGER_TAG).GetComponent<CheckpointManager>();
		foreach (CarController car in _carContainer.GetComponentsInChildren<CarController>(true))
		{
			_carEntered[car] = false;
		}
	}

	/// <summary>
	/// Toggles the entrance.
	/// </summary>
	public void ToggleEntrance(CarController player, int checkpoint){
		_carEntered [player] = true;
		print ("Entrance: " + player.name + checkpoint);
		checkPointManager.UpdateCarCheckpoint(player, checkpoint);
	}

	/// <summary>
	/// Toggles the exit.
	/// </summary>
	/// <param name="player">Player.</param>
	/// <param name="checkpoint">Checkpoint.</param>
	public void ToggleExit(CarController player, int checkpoint){
		if (_carEntered [player]) {
			print ("Exit : " + player.name + checkpoint);
			_carEntered [player] = false;
			checkPointManager.UpdateCarCheckpoint(player, checkpoint);
		}
	}
}
