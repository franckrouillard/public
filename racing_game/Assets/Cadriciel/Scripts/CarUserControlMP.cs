using UnityEngine;

[RequireComponent(typeof(CarController))]
public class CarUserControlMP : MonoBehaviour
{
	private CarController car;  // the car controller we want to use
	private NitroManager nitroManager;
	private RaceManager raceManager;

	[SerializeField]
	private string vertical = "Vertical";

	[SerializeField]
	private string horizontal = "Horizontal";

	[SerializeField]
	private string tilt = "Tilt";

	[SerializeField]
	private string greenShell = "Green Shell";

	[SerializeField]
	private string redShell = "Red Shell";

	[SerializeField]
	private string blueShell = "Blue Shell";

	public bool blockedInput { get; set;}

	
	void Awake ()
	{
		// get the car controller
		car = GetComponent<CarController>();
		nitroManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<NitroManager> ();
		raceManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<RaceManager> ();
		blockedInput = false;
	}
	
	// Update is called once per frame
	void Update () {
		bool jump = CrossPlatformInput.GetButtonDown("Jump");
		bool gs = CrossPlatformInput.GetButtonDown("Green Shell");
		bool rs = CrossPlatformInput.GetButtonDown("Red Shell");
		bool bs = CrossPlatformInput.GetButtonDown("Blue Shell");
		if (jump)
		{
			car.Jump();
		}
		if(gs)
		{
			car.CreateGreenShell(car.transform);
		}
		if(rs)
		{
			car.CreateRedShell(car.transform);
		}
		if(bs)
		{
			car.CreateBlueShell(car.transform);
		}

		car.hasNitro = nitroManager.PlayerHasNitro ();
		car.nitroActivated = Input.GetKey(KeyCode.N);
		if (car.nitroActivated && nitroManager.PlayerHasNitro()) {
			car.consumeNitro (nitroManager.nitroConsumptionRate * nitroManager.nitroReloadSpeed);
		}

		// For debug purpose only
		//if (Input.GetKeyDown(KeyCode.R)) {
		//	raceManager.EndRace();
		//}
	}

	void FixedUpdate()
	{
		if (blockedInput) {
			return;
		}
		
		// pass the input to the car!
		#if CROSS_PLATFORM_INPUT
		float h = CrossPlatformInput.GetAxis(horizontal);
		float v = CrossPlatformInput.GetAxis(vertical);

		float t = CrossPlatformInput.GetAxis(tilt);
		#else
		float h = Input.GetAxis(horizontal);
		float v = Input.GetAxis(vertical);
		bool jump = Input.GetButton("Jump");
		#endif

		car.Move(h,v,t);

	}
}
