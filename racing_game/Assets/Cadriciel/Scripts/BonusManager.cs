﻿using UnityEngine;
using System.Collections;

public class BonusManager : MonoBehaviour {

	[SerializeField] private float rotationSpeed = 50f;
	[SerializeField] private float rotationCenterCorrection = 1f;
	[SerializeField] private float bobbingSpeed = 2f;
	[SerializeField] private float bobbingAmplitude = 1f;

	public AudioClip bonusSound;
	[SerializeField] private float soundVolume = 5f;
	[SerializeField] private float timeBeforeRespawn = 5f;

	[SerializeField] private int scoreValue = 50;
	[SerializeField] private bool willRepairCar = false;
	[SerializeField] private float repairValue = 0.1f;
	[SerializeField] private bool willAddNitroCar = false;
	[SerializeField] private float nitroValue = 0.15f;

	private AudioSource source;

	private Vector3 rotationPoint;

	private NitroManager nitroManager;

	// Use this for initialization
	void Awake () {
		source = GetComponent<AudioSource> ();
		nitroManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<NitroManager> ();
	}

	// Use this for initialization
	void Start () {
		rotationPoint = new Vector3(transform.position.x * rotationCenterCorrection, transform.position.y, transform.position.z);
	}
	
	// Update is called once per frame
	void Update () {
		transform.RotateAround (rotationPoint, new Vector3 (0, 1, 0), rotationSpeed * Time.deltaTime);
		//transform.position += new Vector3(0, Mathf.Sin(bobbingSpeed * Time.time), 0);
		transform.position += bobbingAmplitude*(Mathf.Sin(2*Mathf.PI*bobbingSpeed*Time.time) - Mathf.Sin(2*Mathf.PI*bobbingSpeed*(Time.time - Time.deltaTime)))*transform.up;
	}

	// When a car goes through the collectable object
	void OnTriggerEnter (Collider other) 
	{
		if(!other.attachedRigidbody){
			return;
		}

		CarController car = other.attachedRigidbody.GetComponent<CarController> ();
		if (car)
		{
			// Add score to player
			car.addPoints(scoreValue);
			// Hide the object and disable it
			renderer.enabled = false;
			gameObject.collider.enabled = false;
		
			// Play sound
			source.PlayOneShot (bonusSound, soundVolume);
		
			if(willRepairCar)
			{
				// Repair car for an certain amount
				car.repairCar(repairValue);
			}

			if(willAddNitroCar)
			{
				// Add an certain amount of nitro to the car
				car.addNitro(nitroValue);
			}

			StartCoroutine (enableBonusAfterDisabled());
		}
	}

	IEnumerator enableBonusAfterDisabled()
	{
		// Wait for x seconds before reenable the object
		yield return new WaitForSeconds (timeBeforeRespawn);
		
		// Show and enable object
		renderer.enabled = true;
		gameObject.collider.enabled = true;
	}

}
