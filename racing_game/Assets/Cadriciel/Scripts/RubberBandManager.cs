﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RubberBandManager : MonoBehaviour {

	CheckpointManager checkPointManager;
	List<KeyValuePair<CarController, int>> positions = new List<KeyValuePair<CarController, int>>();

	// Use this for initialization
	void Start () {
		checkPointManager =  GameObject.FindObjectOfType<CheckpointManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		positions = checkPointManager.GetCarPositions ();
		RubberbandFirstPlayers ();

	}

	void RubberbandFirstPlayers(){
		foreach(KeyValuePair<CarController, int> entry in positions){
			entry.Key.rubberbanding = entry.Value >= 3;
		}
	}
}
