﻿using UnityEngine;
using System.Collections;


public class ShellScript : MonoBehaviour {

	[SerializeField]
	private CheckpointManager _manager;

	protected static ShellScript instance;
	GameObject greenShell;
	GameObject redShell;
	GameObject blueShell;

	[SerializeField]
	public float spawnDistanceForward; 
	[SerializeField]
	public float spawnDistanceUp;

	[SerializeField]
	private int GreenShellAvailable = Mathf.CeilToInt(Mathf.Infinity);

	[SerializeField]
	private int RedShellAvailable = 3;

	[SerializeField]
	private int BlueShellAvailable = 1;

	// Use this for initialization
	void Start () {
		instance = this;
		greenShell = (GameObject)Resources.Load ("GreenShell");
		redShell = (GameObject)Resources.Load ("RedShell");
		blueShell = (GameObject)Resources.Load ("BlueShell");
	}

	public static projectile CreateGreenShell(CarController car)
		{
		if (instance.GreenShellAvailable != 0) {
			--instance.GreenShellAvailable;
			Transform carTransform = car.transform;
			GameObject temp = (GameObject)(Instantiate (instance.greenShell, carTransform.position + (instance.spawnDistanceForward * carTransform.forward) + (instance.spawnDistanceUp * carTransform.up), carTransform.rotation));
			GreenShell shell = temp.GetComponent<GreenShell> ();

			shell.Initialize (carTransform);
			return shell;
		}
		return null;
	}

	public static RedShell CreateRedShell(CarController car)
	{
		if (instance.RedShellAvailable != 0) {
			--instance.RedShellAvailable;
			Transform carTransform = car.transform;
			GameObject temp = (GameObject)(Instantiate (instance.redShell, carTransform.position + (instance.spawnDistanceForward * carTransform.forward) + (instance.spawnDistanceUp * carTransform.up), carTransform.rotation));
			RedShell shell = temp.GetComponent<RedShell> ();

			// Détermination de la proie
			instance._manager.UpdateCarPositions ();
			int index = instance._manager.GetCarPosition (car);

			// si one n'est pas en première position
			if (index != 0) {

				CarController target = instance._manager.GetCarAtPosition (index - 1);
				shell.Initialize (carTransform, target.transform, true);
				shell.SetWaypoint (instance._manager.GetCarCheckpoint (car));
			} else
				shell.Initialize (carTransform, null, false);

			return shell;
		}
		return null;
	}

	public static BlueShell CreateBlueShell(CarController car)
	{
		if (instance.BlueShellAvailable != 0) {
			--instance.BlueShellAvailable;
			Transform carTransform = car.transform;
			GameObject temp = (GameObject)(Instantiate (instance.blueShell, carTransform.position + (instance.spawnDistanceForward * carTransform.forward) + (instance.spawnDistanceUp * carTransform.up), carTransform.rotation));
			BlueShell shell = temp.GetComponent<BlueShell> ();

			// Détermination de la proie
			instance._manager.UpdateCarPositions ();
			int index = instance._manager.GetCarPosition (car);
		
			// si one n'est pas en première position
			if (index != 0) {
				CarController target = instance._manager.GetCarAtPosition (0);
				shell.Initialize (carTransform, target.transform, true);
				shell.SetWaypoint (instance._manager.GetCarCheckpoint (car));
			} else
				shell.Initialize (carTransform, null, false);

			return shell;
		}
		return null;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
