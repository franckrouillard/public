﻿using UnityEngine;
using System.Collections;

public class DamageManager : MonoBehaviour {

	[SerializeField] private Texture _noDamage;
	[SerializeField] private Texture _damage;
	[SerializeField] private Texture _damageContainer;
	[SerializeField] private GameObject _damageLabel;
	[SerializeField] GameObject _player1;
	[SerializeField] GameObject _player2;

	public float _cumulatedDamage { get; set;}
	private string _damageMessage = "dommage au véhicule";
	private string _damageMessageFull = "dommage au véhicule [100%]";
	private Vector2 _player1DamageContainerPosition = new Vector2(8f, 450f);

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		_cumulatedDamage = _player1.GetComponent<CarController>().CarDamage;
		_player1DamageContainerPosition = new Vector2(Screen.width*0.005f ,Screen.height*0.80f);
	}

	void OnGUI () {
		float damageProgress = (_damageContainer.width - 8f) - (_cumulatedDamage * (_damageContainer.width - 8f));
		string message = _damageMessage;

		// Gauge
		GUI.BeginGroup (new Rect(_player1DamageContainerPosition.x, _player1DamageContainerPosition.y, _damageContainer.width, _damageContainer.height));
		GUI.DrawTexture (new Rect(0f, 0f, _damageContainer.width, _damageContainer.height), _damageContainer);

		// Fixed no damage
		GUI.BeginGroup (new Rect(4f, 4f, _damageContainer.width - 8f, _damageContainer.height-8f));
		GUI.DrawTexture (new Rect(0f, 0f, _damageContainer.width, _damage.height), _damage);
		GUI.EndGroup ();

		// Damage level
		GUI.BeginGroup (new Rect(4f, 4f, damageProgress, _damageContainer.height-8f));
		GUI.DrawTexture (new Rect(0f, 0f, _damageContainer.width, _damage.height), _noDamage);
		GUI.EndGroup ();

		if (_cumulatedDamage == 1f) {
			message = _damageMessageFull;
		}
		_damageLabel.GetComponent<GUIText> ().text = message;
		_damageLabel.GetComponent<GUIText> ().pixelOffset = new Vector2 (_player1DamageContainerPosition.x, -_player1DamageContainerPosition.y + 30f);
		
		GUI.EndGroup ();
	}

}
