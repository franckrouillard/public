﻿using UnityEngine;
using System.Collections;

public class GreenShell : projectile
{
	private Transform _transform;

	[SerializeField]
	private float Speed = 60.0f;

	// Use this for initialization
	void Start () {
		_transform = transform;
	}

	public void Initialize(Transform vehicule)
	{
		isTracking = false;
		target = null;
		hits = 3;

		rigidbody.velocity = Speed * vehicule.forward;

		StartCoroutine (TimerBeforeDeath());
	}

	// Update is called once per frame
	void Update () {
		//rigidbody.AddForce(new Vector3(0, 4f * Physics.gravity.y, 0));

		// Safety check to destroy shell if it is out of bound
		if (rigidbody.position.y < -100f) {
			Destroy (this.gameObject);
		}
	}

	void OnCollisionEnter(Collision collision) {

		if (collision.gameObject.tag == "Player") {

			// Rekt the player
			collision.gameObject.rigidbody.velocity = Vector3.zero;
			collision.gameObject.rigidbody.AddTorque (new Vector3 (0, 5000f, 0));

			// Add damage to player
			if(collision.gameObject.GetComponentInParent<CarController>()){
				collision.gameObject.GetComponentInParent<CarController>().damageCar(0.20f);
			}

			// Destroy shell
			Destroy (this.gameObject);
		} else if (collision.gameObject.tag == "Stationary") {
			if (--hits == 0) {
				Destroy (this.gameObject);
			}

		}
		else if(collision.gameObject.GetComponent<ToggleShortcut>()){
			collision.gameObject.GetComponent<ToggleShortcut>().DamageShortcut();
		}
	}

	IEnumerator TimerBeforeDeath()
	{
		// Wait for x seconds before reenable the object
		yield return new WaitForSeconds (20f);
		
		// Detroy the shell object
		Destroy (this.gameObject);
	}
}
