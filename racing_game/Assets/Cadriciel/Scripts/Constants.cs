using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
	public static Vector2 ANGLES_FOR_SPEED = new Vector2(-130f, 122f);
	public static float MAX_SPEED = 260f;
	public static float NITRO_SPEED_FACTOR = 1.5f;
	public static float VISUAL_CUE_TIMEOUT = 2f;

	public static string GAME_MANAGER_TAG = "GameController";
	public static List<string> SHELL_NAMES =  new List<string>(new string[] { "BlueShell", "RedShell", "GreenShell" });
}

