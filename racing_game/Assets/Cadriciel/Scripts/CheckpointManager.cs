﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CheckpointManager : MonoBehaviour 
{

	[SerializeField]
	private GameObject _carContainer;

	[SerializeField]
	private int _checkPointCount;
	[SerializeField]
	private int _totalLaps;

	private bool _finished = false;
	int _nbCars;

	private Checkpoint[] _Checkpoints;
	
	private Dictionary<CarController,PositionData> _carPositions = new Dictionary<CarController, PositionData>();

	private class PositionData
	{
		public int lap;
		public int checkPoint;
		public int position;
	}

	// Use this for initialization
	void Awake () 
	{
		foreach (CarController car in _carContainer.GetComponentsInChildren<CarController>(true))
		{
			_carPositions[car] = new PositionData();
			_carPositions[car].checkPoint = -1;
		}
		_nbCars = _carContainer.transform.childCount;

		 GameObject cp = GameObject.Find("Checkpoints");
		_Checkpoints = cp.GetComponentsInChildren<Checkpoint>();
	}
	
	public void CheckpointTriggered(CarController car, int checkPointIndex)
	{

		PositionData carData = _carPositions[car];

		if (!_finished)
		{
			if (checkPointIndex == 0 && carData.checkPoint != -1 )
			{
				if (carData.checkPoint == _checkPointCount-1)
				{
					carData.checkPoint = checkPointIndex;
					carData.lap += 1;
					Debug.Log(car.name + " lap " + carData.lap);
					if (carData.lap >= _totalLaps)
					{
						_finished = true;
						GetComponent<RaceManager>().EndRace();
					}

					if (IsPlayer(car) && carData.lap < _totalLaps)
					{
						GetComponent<RaceManager>().Announce("tour " + (carData.lap+1).ToString());
					}
				}
			}
			else if (carData.checkPoint == checkPointIndex-1) //Checkpoints must be hit in order
			{
				carData.checkPoint = checkPointIndex;
			}
			UpdateCarPositions();
		}


	}

	bool IsPlayer(CarController car)
	{
		return car.GetComponent<CarUserControlMP>() != null;
	}

	public int GetPlayerCheckPoint(Transform player){
		return _carPositions [player.GetComponent<CarController> ()].checkPoint;
	}

	public void UpdateCarPositions(){
		//string message = "Positions :";
		List<KeyValuePair<CarController,float>> carProgress = new List<KeyValuePair<CarController,float>>();
		foreach(KeyValuePair<CarController,PositionData> entry in _carPositions){

			if(entry.Key.gameObject.activeSelf){
				int checkpoint = (entry.Value.checkPoint == -1) ? 19 : entry.Value.checkPoint ;
				float distance = 1000000 * entry.Value.lap + 1000 * entry.Value.checkPoint + Mathf.Abs(Vector3.Distance(entry.Key.transform.position, _Checkpoints[checkpoint].transform.position));
				carProgress.Add(new KeyValuePair<CarController,float>(entry.Key, distance));
			}
		}
		carProgress.Sort ((x,y) => -1 * x.Value.CompareTo (y.Value));

		for(int i = 0  ; i < carProgress.Count ; ++i) {
			_carPositions[carProgress[i].Key].position = i;
		}
	}

	public void UpdateCarCheckpoint(CarController car, int checkPoint){
		_carPositions [car].checkPoint = checkPoint;
	}

	public CarController GetCarAtPosition(int position){
		foreach(KeyValuePair<CarController,PositionData> entry in _carPositions){
			if(entry.Value.position == position){
				return entry.Key;
			}
		}
		return null;
	}
	
	public int GetCarPosition(CarController car){
		return _carPositions [car].position;
	}

	public List<KeyValuePair<CarController, int>> GetCarPositions(){
		List<KeyValuePair<CarController, int>> positions = new List<KeyValuePair<CarController, int>>();
		foreach(KeyValuePair<CarController,PositionData> entry in _carPositions){
			positions.Add(new KeyValuePair<CarController, int>(entry.Key, entry.Value.position));
		}
		return positions;
	}

	public Dictionary<int, CarController> GetFinalCarPositions(){
		Dictionary<int, CarController> positions = new Dictionary<int, CarController>();
		foreach(KeyValuePair<CarController,PositionData> entry in _carPositions){
			if(entry.Key.gameObject.activeSelf)
				positions.Add(entry.Value.position, entry.Key);
		}
		return positions;
	}

	public int GetCarCheckpoint(CarController car){
		return _carPositions [car].checkPoint;
	}
}
