﻿using UnityEngine;
using System.Collections;

public class NitroManager : MonoBehaviour {

	[SerializeField] private Texture _nitro;
	[SerializeField] private Texture _nitroContainer;
	[SerializeField] private GameObject _nitroLabel;
	[SerializeField] GameObject _player1;
	[SerializeField] public float nitroReloadSpeed = .001f;
	[SerializeField] public float nitroConsumptionRate = 3f;

	private float _cumulatedNitro;
	private GUIText _nitroTag;
	private string _nitroMessage = "nitro";
	private string _nitroMessageFull = "nitro [100%]";
	private Vector2 _player1NitroContainerPosition;

	public float PlayerMaxSpeed { get; set;}

	// Use this for initialization
	void Start () {
		//_cumulatedNitro = _player1.GetComponent<CarController> ().cumulatedNitro;
		_player1.GetComponent<CarController> ().cumulatedNitro = 0f;
		PlayerMaxSpeed = _player1.GetComponent<CarController> ().MaxSpeed;
		_nitroTag = _nitroLabel.GetComponentInChildren<GUIText>();
	}

	void FixedUpdate(){
		_player1.GetComponent<CarController> ().addNitro(nitroReloadSpeed);
		_player1NitroContainerPosition = new Vector2(Screen.width*0.005f ,Screen.height*0.90f);
	}
	
	void OnGUI () {
		float nitroProgress = _player1.GetComponent<CarController> ().cumulatedNitro * (_nitroContainer.width - 8f);
		string message = _nitroMessage;
		GUI.BeginGroup (new Rect(_player1NitroContainerPosition.x, _player1NitroContainerPosition.y, _nitroContainer.width, _nitroContainer.height));
			GUI.DrawTexture (new Rect(0f, 0f, _nitroContainer.width, _nitroContainer.height), _nitroContainer);
			GUI.BeginGroup (new Rect(4f, 4f, nitroProgress, _nitroContainer.height-8f));
				GUI.DrawTexture (new Rect(0f, 0f, _nitroContainer.width, _nitro.height), _nitro);
			GUI.EndGroup ();
		if (_player1.GetComponent<CarController> ().cumulatedNitro == 1f) {
			message = _nitroMessageFull;
		}
		_nitroTag.text = message;
		_nitroTag.pixelOffset = new Vector2(_player1NitroContainerPosition.x, -_player1NitroContainerPosition.y + 30f);
		GUI.EndGroup ();
	}

	public bool PlayerHasNitro(){
		return _player1.GetComponent<CarController> ().cumulatedNitro > 0.001f;
	}
}
