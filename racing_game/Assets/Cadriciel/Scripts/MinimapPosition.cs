﻿using UnityEngine;
using System.Collections;

public class MinimapPosition : MonoBehaviour {

	[SerializeField] private GameObject PositionJoueur1;
	[SerializeField] private GameObject PositionVoitureBlanche;
	[SerializeField] private GameObject PositionVoitureJaune;
	[SerializeField] private GameObject PositionVoitureRouge;
	[SerializeField] private GameObject PositionVoitureBleue;
	[SerializeField] private GameObject PositionVoitureVerte;
	[SerializeField] private GameObject PositionVoitureOrange;
	[SerializeField] private GameObject _VoitureJoueur1;
	[SerializeField] private GameObject _VoitureBlanche;
	[SerializeField] private GameObject _VoitureJaune;
	[SerializeField] private GameObject _VoitureRouge;
	[SerializeField] private GameObject _VoitureBleue;
	[SerializeField] private GameObject _VoitureVerte;
	[SerializeField] private GameObject _VoitureOrange;

	Vector3 position;

	// Use this for initialization
	void Start () {
	
	}

	void FixedUpdate () {

	}

	void OnGUI () {
		PositionJoueur1.GetComponent<GUITexture> ().pixelInset = new Rect(0.2f*(_VoitureJoueur1.transform.position.x - 283.4431f), -100f + 0.2f*(_VoitureJoueur1.transform.position.z - 85.596f), 8f, 8f);
		PositionVoitureBlanche.GetComponent<GUITexture> ().pixelInset = new Rect(0.2f*(_VoitureBlanche.transform.position.x - 283.4431f), -100f + 0.2f*(_VoitureBlanche.transform.position.z - 85.596f), 8f, 8f);
		PositionVoitureJaune.GetComponent<GUITexture> ().pixelInset = new Rect(0.2f*(_VoitureJaune.transform.position.x - 283.4431f), -100f + 0.2f*(_VoitureJaune.transform.position.z - 85.596f), 8f, 8f);
		PositionVoitureRouge.GetComponent<GUITexture> ().pixelInset = new Rect(0.2f*(_VoitureRouge.transform.position.x - 283.4431f), -100f + 0.2f*(_VoitureRouge.transform.position.z - 85.596f), 8f, 8f);
		PositionVoitureBleue.GetComponent<GUITexture> ().pixelInset = new Rect(0.2f*(_VoitureBleue.transform.position.x - 283.4431f), -100f + 0.2f*(_VoitureBleue.transform.position.z - 85.596f), 8f, 8f);
		PositionVoitureVerte.GetComponent<GUITexture> ().pixelInset = new Rect(0.2f*(_VoitureVerte.transform.position.x - 283.4431f), -100f + 0.2f*(_VoitureVerte.transform.position.z - 85.596f), 8f, 8f);
		PositionVoitureOrange.GetComponent<GUITexture> ().pixelInset = new Rect(0.2f*(_VoitureOrange.transform.position.x - 283.4431f), -100f + 0.2f*(_VoitureOrange.transform.position.z - 85.596f), 8f, 8f);
	}
}
