using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OutOfBound : MonoBehaviour {

	[SerializeField]
	private CheckpointManager _manager;
	[SerializeField]
	GameObject disabledScreenTexture;
	[SerializeField]
	float disabledTime = 3f;

	List<Transform> checkPoints = new List<Transform>();

	// Use this for initialization
	void Start () {
		Transform checkPointsParent = GameObject.Find ("Checkpoints").transform;
		foreach(Transform checkPoint in checkPointsParent){
			checkPoints.Add(checkPoint);
		}
		disabledScreenTexture.guiTexture.enabled = false;
	}

	void OnTriggerEnter(Collider other)
	{
		GameObject car = FindParentPlayer (other.gameObject);
		if (car) {
			int checkPoint = _manager.GetPlayerCheckPoint(car.transform);
			checkPoint = checkPoint < 0 ? 0 : checkPoint;
			ReplacePlayer(car.transform, checkPoints[checkPoint]);
		}
	}

	// in case the player was too fast
	void OnTriggerStay(Collider other){
		GameObject car = FindParentPlayer (other.gameObject);
		if (car) {
			int checkPoint = _manager.GetPlayerCheckPoint(car.transform);
			checkPoint = checkPoint < 0 ? 0 : checkPoint;
			ReplacePlayer(car.transform, checkPoints[checkPoint]);
		}
	}

	GameObject FindParentPlayer(GameObject childObject){
		Transform child = childObject.transform;
		while(child.parent){
			if(child.parent.tag == "Player"){
				return child.parent.gameObject;
			}
			child = child.parent;
		}
		return null;
	}

	bool CheckHuman(Transform player){
		return player.GetComponent<CarUserControlMP> ();
	}

	void ReplacePlayer(Transform player, Transform checkpoint){
		player.rigidbody.isKinematic = true;
		player.position = checkpoint.position;
		player.rotation = checkpoint.rotation;
		
		WheelCollider[] childrenRigidbodies = player.gameObject.GetComponentsInChildren<WheelCollider> ();
		foreach(WheelCollider r in childrenRigidbodies){
			r.brakeTorque = Mathf.Infinity;
		}

		if (CheckHuman (player)) {
			StartCoroutine (StopPlayer (player));
		} else {
			StartCoroutine (StopAI (player));
		}
	}

	IEnumerator StopPlayer(Transform player){
		CarUserControlMP playerController = player.GetComponent<CarUserControlMP> ();
		playerController.blockedInput = true;
		disabledScreenTexture.guiTexture.pixelInset = new Rect (-Screen.height *0.333f/2f, -Screen.height *0.333f/2f, Screen.height * 0.333f, Screen.height * 0.333f);

		disabledScreenTexture.guiTexture.enabled = true;
		//grayedScreenTexture.guiTexture.color = grayedScreenTexture.guiTexture.color;
		yield return new WaitForSeconds (disabledTime);
		playerController.blockedInput = false;
		player.rigidbody.isKinematic = false;
		//grayedScreenTexture.guiTexture.color = grayedScreenTexture.guiTexture.color;
		disabledScreenTexture.guiTexture.enabled = false;
	}
	IEnumerator StopAI(Transform ai){
		yield return new WaitForSeconds (disabledTime);
		ai.rigidbody.isKinematic = false;
	}
}
