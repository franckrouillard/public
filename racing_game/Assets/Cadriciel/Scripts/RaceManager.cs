﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class RaceManager : MonoBehaviour 
{
	// Serializable fields
	[SerializeField] private GameObject _carContainer;
	[SerializeField] private GUIText _announcement;
	[SerializeField] private GUIText _title;
	[SerializeField] private GUIText _leaderboard;
	[SerializeField] private int _timeToStart;
	[SerializeField] private int _endCountdown;
	[SerializeField] public AudioClip countdownClip;
	[SerializeField] public AudioClip musicClip;
	[SerializeField] public AudioClip endMusicClip;

	// Audio
	AudioSource countdown;
	AudioSource music;
	AudioSource endMusic;

	// Leaderboard elements
	private GUIStyle backgroundStyle = new GUIStyle();
	private Texture2D fadeTexture;
	private Color deltaColor = new Color(0,0,0,0);
	private Color currentScreenColor = new Color(0,0,0,0);
	private Color targetScreenColor = new Color(0,0,0,0);

	// Time
	private float beginRaceTime;
	private float endRaceTime;
	bool end = false;

	// Handle
	private CheckpointManager checkPointManager;
	private GameObject trophy;


	// Use this for initialization
	void Awake () 
	{
		// Get handles
		checkPointManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<CheckpointManager> ();
		trophy = GameObject.FindGameObjectWithTag("Trophy");

		// Block car control
		CarActivation(false);

		// setup the sound source
		countdown = SetUpCountdownSource (countdownClip);
		music = SetUpMusicSource (musicClip);
		endMusic = SetUpMusicSource (endMusicClip);

		// Initialize leaderboard values
		fadeTexture = new Texture2D(1, 1);        
		backgroundStyle.normal.background = fadeTexture;
		SetScreenColor(currentScreenColor);
	}
	
	void Start()
	{
		StartCoroutine(StartCountdown());
	}

	IEnumerator StartCountdown()
	{
		int count = _timeToStart;
		do 
		{
			if(count.ToString() == "3")
			{
				countdown.PlayOneShot(countdownClip);
			}
			_announcement.text = count.ToString();
			yield return new WaitForSeconds(1.0f);
			count--;
		}
		while (count > 0);
		beginRaceTime = Time.time;
		_announcement.text = "partez!";
		CarActivation(true);
		music.Play();
		yield return new WaitForSeconds(1.0f);
		_announcement.text = "";
	}

	public void EndRace()
	{
		endRaceTime = Time.time;

		string time = (endRaceTime - beginRaceTime).ToString("#.00");

		music.Stop();
		endMusic.Play();
		trophy.renderer.enabled = true;
		CarActivation(false);
		GameObject.FindGameObjectWithTag ("Minimap").SetActive (false);

		// Fill leaderboard
		Dictionary<int, CarController> classement = checkPointManager.GetFinalCarPositions();
		_title.text = "CLASSEMENT : \n\n";
		for (int i = 0; i < classement.Count; i++)
		{
			_leaderboard.text += (i+1) + " : " + classement[i].name.ToLower();
			if(i ==0)
			{
				_leaderboard.text += "  -  " + time + " secondes";
			}
			_leaderboard.text += "\n";
			for (int j = 0; j < i+1; j++)
			{
				_leaderboard.text += "  ";
			}
		}		
		_leaderboard.fontSize = 50;

		end = true;
	}

	private void OnGUI()
	{   
		if (end)
		{
			FadeColor (new Color (0, 0, 0, 0.6f), 1.0f);
			if(currentScreenColor == currentScreenColor)
			{
				GUIStyle buttonStyle = new GUIStyle(GUI.skin.button);
				buttonStyle.fontSize = 30;
				buttonStyle.normal.textColor = Color.white;
				buttonStyle.fontStyle = FontStyle.Bold;
				if (GUI.Button (new Rect (Screen.width/2 - 150,Screen.height - 75 ,300,50), "Menu principal", buttonStyle))
				{
					endMusic.Stop();
					Application.LoadLevel("boot");
				}
				if (GUI.Button (new Rect (Screen.width/2 - 150,Screen.height- 125 ,300,50), "Recommencer", buttonStyle))
				{
					endMusic.Stop();
					Application.LoadLevel("course");
				}
			}

			// if the current color of the screen is not equal to the desired color: keep fading!
			if (currentScreenColor != targetScreenColor)
			{			
				SetScreenColor(currentScreenColor + (2*deltaColor) * Time.deltaTime);
			}
			
			// only draw the texture when the alpha value is greater than 0:
			if (currentScreenColor.a > 0)
			{			
				GUI.depth = -1000;
				GUI.Label(new Rect(-10, -10, Screen.width + 10, Screen.height + 10), fadeTexture, backgroundStyle);
			}
		} 

	}

	public void Announce(string announcement, float duration = 2.0f)
	{
		StartCoroutine(AnnounceImpl(announcement,duration));
	}

	IEnumerator AnnounceImpl(string announcement, float duration)
	{
		_announcement.text = announcement;
		yield return new WaitForSeconds(duration);
		_announcement.text = "";
	}

	public void CarActivation(bool activate)
	{
		foreach (CarAIControl car in _carContainer.GetComponentsInChildren<CarAIControl>(true))
		{
			car.enabled = activate;
		}
		
		foreach (CarUserControlMP car in _carContainer.GetComponentsInChildren<CarUserControlMP>(true))
		{
			car.enabled = activate;
		}
	}
	
	public void SetScreenColor(Color newScreenOverlayColor)
	{
		currentScreenColor = newScreenOverlayColor;
		fadeTexture.SetPixel(0, 0, currentScreenColor);
		fadeTexture.Apply();
	}

	public void FadeColor(Color newScreenOverlayColor, float fadeDuration)
	{
		if (fadeDuration <= 0.0f)
		{
			SetScreenColor(newScreenOverlayColor);
		}
		else
		{
			targetScreenColor = newScreenOverlayColor;
			deltaColor = (targetScreenColor - currentScreenColor) / fadeDuration;
		}
	}

	// sets up and adds new audio source to the game object
	AudioSource SetUpMusicSource(AudioClip clip)
	{
		AudioSource source = gameObject.AddComponent<AudioSource>();
		source.clip = clip;
		source.loop = true;
		source.volume = 0.5f;
		return source;
	}

	// sets up and adds new audio source to the game object
	AudioSource SetUpCountdownSource(AudioClip clip)
	{
		AudioSource source = gameObject.AddComponent<AudioSource>();
		source.clip = clip;
		source.loop = false;
		source.volume = 0.75f;
		return source;
	}
}
