﻿using UnityEngine;
using System.Collections;

public class RedShell : projectile
{


	private Transform _transform;

	[SerializeField]
	private float Speed = 60.0f;

	[SerializeField]
	private float Radius = 25.0f;

	WaypointCircuit _circuit;
	private Transform waypoint;
	private int waypointIndex;
	
	// Use this for initialization
	void Start () {
		_transform = transform;
	}
	
	public void Initialize(Transform car, Transform target, bool isTracking)
	{
		this.isTracking = true;
		this.target = target;
		hits = 1;

		GameObject wp = GameObject.Find ("AI paths");
		_circuit = wp.GetComponentInChildren<WaypointCircuit> ();

		if (target == null) {
			rigidbody.velocity = Speed * car.forward;
		}

		StartCoroutine (TimerBeforeDeath());
	}

	public void SetWaypoint(int checkpoint)
	{
		if (checkpoint < 0)
			checkpoint = 19;

		waypointIndex = (checkpoint + 1)%19;

		waypoint = _circuit.Waypoints[waypointIndex];
		Debug.Log ("checkpoint auto " + checkpoint);
		Debug.Log ("waypoint destinataire " + waypoint);
		Debug.Log ("target auto " + target);
		rigidbody.velocity = Speed * Vector3.Normalize(waypoint.position - transform.position);
	}
	
	// Update is called once per frame
	void Update () {
		//rigidbody.AddForce(new Vector3(0, 4f * Physics.gravity.y, 0));
		
		// Safety check to destroy shell if it is out of bound
		if (rigidbody.position.y < -100f) {
			Destroy (this.gameObject);
		}

		if (target == null)
			return;

		// si distance entre shell et auto acceptable
		if (Vector3.Distance (_transform.position, target.position) <= Radius)
			rigidbody.velocity = Speed * Vector3.Normalize(target.position - _transform.position);
		else if (Vector3.Distance (_transform.position, waypoint.position) <= 15.0f) 
		{
			waypointIndex = (++waypointIndex)%19;
			waypoint = _circuit.Waypoints[ waypointIndex];
			rigidbody.velocity = Speed * Vector3.Normalize(waypoint.position - transform.position);
		}
		else
			rigidbody.velocity = Speed * Vector3.Normalize(waypoint.position - transform.position);


	}
	
	void OnCollisionEnter(Collision collision) {
		
		if (collision.gameObject.tag == "Player") {
			
			// Rekt the player
			collision.gameObject.rigidbody.velocity = Vector3.zero;
			collision.gameObject.rigidbody.AddTorque (new Vector3 (0, 5000f, 0));
			
			// Add damage to player
			if(collision.gameObject.GetComponentInParent<CarController>()){
				collision.gameObject.GetComponentInParent<CarController>().damageCar(0.20f);
			}
			
			// Destroy shell
			Destroy (this.gameObject);
		} else if (collision.gameObject.tag == "Stationary") {
			if (--hits == 0) {
				Destroy (this.gameObject);
			}
			
		}
		else if(collision.gameObject.GetComponent<ToggleShortcut>()){
			collision.gameObject.GetComponent<ToggleShortcut>().DamageShortcut();
		}
	}

	IEnumerator TimerBeforeDeath()
	{
		// Wait for x seconds before reenable the object
		yield return new WaitForSeconds (30f);
		
		// Detroy the shell object
		Destroy (this.gameObject);
	}
}