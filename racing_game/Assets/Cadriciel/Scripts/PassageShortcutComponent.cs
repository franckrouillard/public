﻿using UnityEngine;
using System.Collections;

public class PassageShortcutComponent : MonoBehaviour {
	public enum ShortcutState {entrance, exit};
	
	[SerializeField] int lastCheckPoint;
	[SerializeField] ShortcutState state;

	private PassageShortcut shortcutMaster;
	
	// Use this for initialization
	void Start () {
		shortcutMaster = transform.GetComponentInParent<PassageShortcut> ();
	}

	void OnTriggerEnter(Collider other){
		CarController player = other.GetComponentInParent<CarController> ();
		if(player){
			ToggleShortcut(player);
		}
	}
	
	void OnCollisionEnter(Collision collision){
		CarController player = collision.collider.GetComponentInParent<CarController> ();
		if(player){
			ToggleShortcut(player);
		}
	}

	void ToggleShortcut(CarController player){
		if(state == ShortcutState.entrance){
			shortcutMaster.ToggleEntrance(player, lastCheckPoint);
		}
		else if(state == ShortcutState.exit){
			shortcutMaster.ToggleExit(player, lastCheckPoint);
		}
	}
}
