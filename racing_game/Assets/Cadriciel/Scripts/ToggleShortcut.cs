﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class ToggleShortcut : MonoBehaviour {

	[SerializeField] int health;
	[SerializeField] int lastCheckPoint;
	[SerializeField] bool stayOpen = false;
	private int originalHealth;
	private bool opened = false;
	CheckpointManager checkPointManager;

	// Use this for initialization
	void Start () {
		originalHealth = health;
		checkPointManager = GameObject.FindGameObjectWithTag (Constants.GAME_MANAGER_TAG).GetComponent<CheckpointManager>();
	}

	void FixedUpdate(){
		if(stayOpen){
			return;
		}

		if (health <= 0) {
			opened = true;
			StartCoroutine(OpenShortcut());
		}
	}

	void OnTriggerEnter(Collider other){

		if(opened || stayOpen){
			if(isPlayer(other)){
				checkPointManager.UpdateCarCheckpoint(other.GetComponentInParent<CarController>(), lastCheckPoint);
			}
			Physics.IgnoreCollision(transform.collider, other);
			return;
		}
		if(isPlayer(other) || isShell(other)){
			DamageShortcut();
		}
	}

	IEnumerator OpenShortcut(){
		gameObject.renderer.enabled = false;
		yield return new WaitForSeconds(60f);
		gameObject.renderer.enabled = true;
		health = originalHealth;
	}

	bool isPlayer(Collider collider){
		return collider.GetComponentInParent<CarController>();
	}

	bool isShell(Collider other){
		foreach(string shellName in Constants.SHELL_NAMES){
			if(other.transform.name.Contains(shellName)){
				return true;
			}
		}
		return false;
	}

	public void DamageShortcut(){
		health -= 1;
	}
}
