﻿using UnityEngine;
using System.Collections;

public class TurnClue : MonoBehaviour {
	public enum SignSize{small, medium, big};

	private bool displayArrow;
	[SerializeField] SignSize arrowSize = SignSize.medium;
	[SerializeField] bool left;
	[SerializeField] Texture arrowTexture;

	private bool showArrow = true;
	private bool shouldShowArrow = false;
	private float maxArrowSize = 128f;
	private float minArrowSize = 32f;
	private float timer = 0f;
	private float waitTime = .7f;

	void Awake(){
		timer = waitTime;
	}

	void Update(){

		timer -= Time.deltaTime;
		if(timer <= 0f){
			showArrow = !showArrow;
			timer = waitTime;
		}
	}

	void OnTriggerEnter(Collider other){
		if (other.transform.GetComponentInParent<CarUserControlMP> () &&
		    Vector3.Angle(other.transform.forward, transform.forward) <= 90f) {
			StartCoroutine(FlashArrow());
		}
	}

	void OnGUI(){
		float size = 0f;
		switch (arrowSize) {
		case SignSize.small:
			size = 32f;
			break;
		case SignSize.medium:
			size = 128f;
			break;
		case SignSize.big:
			size = 512f;
			break;
		default:
			size = 128f;
			break;
		}
		if(showArrow && shouldShowArrow) {
			if(left){
				GUI.DrawTexture (new Rect(8f, (Screen.height-size)/2f, size, size), arrowTexture);
			}else{
				GUI.DrawTexture (new Rect(Screen.width - 8f, (Screen.height-size)/2f, -size, size), arrowTexture);
			}

		}
	}

	IEnumerator FlashArrow(){
		shouldShowArrow = true;
		yield return new WaitForSeconds (3.5f);
		shouldShowArrow = false;
	}
}
