﻿using UnityEngine;
using System.Collections;

public class SpeedBonus : MonoBehaviour {

	[SerializeField] [Range(1, 1.5f)] private float speedAccelerationFactor = 1.05f;
	[SerializeField] private Material _speedBonus;
	[SerializeField] private Material _speedBonusActivated;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

	}

	// When a car touches the speed bonus
	void OnCollisionEnter(Collision other)
	{
		if(!other.collider.attachedRigidbody){
			return;
		}
		
		CarController car = other.collider.attachedRigidbody.GetComponent<CarController> ();
		if (car)
		{
			renderer.material = _speedBonusActivated;
		}
	}

	// When a car goes through the speed bonus
	void OnCollisionStay(Collision other)
	{
		if(!other.collider.attachedRigidbody){
			return;
		}
		
		CarController car = other.collider.attachedRigidbody.GetComponent<CarController> ();
		if (car)
		{
			car.rigidbody.velocity *= speedAccelerationFactor;
		}
	}

	// When a car leaves the speed bonus
	void OnCollisionExit(Collision other)
	{
		if(!other.collider.attachedRigidbody){
			return;
		}
		
		CarController car = other.collider.attachedRigidbody.GetComponent<CarController> ();
		if (car)
		{
			renderer.material = _speedBonus;
		}
	}
}
