﻿using UnityEngine;
using System.Collections;

public class CupManager : MonoBehaviour {

	[SerializeField] private float rotationSpeed = 50f;
	private Vector3 rotationPoint;

	// Use this for initialization
	void Start () {
	}

	void Update () {
		if(renderer.enabled == true)
		{
			transform.Rotate(new Vector3(0, 1, 0), rotationSpeed * Time.deltaTime);
		}
	}
}
