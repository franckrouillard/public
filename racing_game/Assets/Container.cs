﻿using UnityEngine;
using System.Collections;

public class Container : MonoBehaviour {

	public Transform startMarker;
	public Transform endMarker;
	public float speed = 2.0F;
	private float startTime;
	private float journeyLength;

	void Start() {
		startTime = Time.time;
		journeyLength = Vector3.Distance(startMarker.position, endMarker.position);
	}
	void Update() {
		float distCovered = (Time.time - startTime) * speed;
		float fracJourney = distCovered / journeyLength;
		transform.position = Vector3.Lerp(startMarker.position, endMarker.position, fracJourney);

		if (transform.position == endMarker.position) {
			endMarker.position = startMarker.position;
			startMarker.position = transform.position;
			startTime = Time.time;
			journeyLength = Vector3.Distance(startMarker.position, endMarker.position);
		}
	}

	void OnCollisionEnter(Collision col)
	{
		Transform auto = col.transform;
		Rigidbody body = col.rigidbody;

		//body.velocity *= -1;

	}
}
