/* Code pour le l'algorithme de
 *
 *
 *
 *
 */


#include <vector>
#include <iostream>
#include <fstream>
#include <math.h>
#include <sys/time.h>
#include <algorithm>
#include <cmath>

int leafsize;

void sum(std::vector< std::vector<int> > &A, std::vector< std::vector<int> > &B, std::vector< std::vector<int> > &C, int tam)
{
    int i, j;

    for (i = 0; i < tam; i++) {
        for (j = 0; j < tam; j++) {
            C[i][j] = A[i][j] + B[i][j];
        }
    }
}

void subtract(std::vector< std::vector<int> > &A,
              std::vector< std::vector<int> > &B,
              std::vector< std::vector<int> > &C, int tam) {
    int i, j;

    for (i = 0; i < tam; i++) {
        for (j = 0; j < tam; j++) {
            C[i][j] = A[i][j] - B[i][j];
        }
    }
}

void ikjalgorithm(std::vector< std::vector<int> > A,
                                   std::vector< std::vector<int> > B,
                                   std::vector< std::vector<int> > &C, int n) {
    for (int i = 0; i < n; i++) {
        for (int k = 0; k < n; k++) {
            for (int j = 0; j < n; j++) {
                C[i][j] += A[i][k] * B[k][j];
            }
        }
    }
}

void strassenR(std::vector< std::vector<int> > &A,
              std::vector< std::vector<int> > &B,
              std::vector< std::vector<int> > &C, int tam) {

    if (tam <= leafsize) {
            ikjalgorithm(A, B, C, tam);
            return;
    }

    // other cases are treated here:
    else {

        int newTam = tam/2;
        std::vector<int> inner (newTam);
        std::vector< std::vector<int> >
            a11(newTam,inner), a12(newTam,inner), a21(newTam,inner), a22(newTam,inner),
            b11(newTam,inner), b12(newTam,inner), b21(newTam,inner), b22(newTam,inner),
              c11(newTam,inner), c12(newTam,inner), c21(newTam,inner), c22(newTam,inner),
            p1(newTam,inner), p2(newTam,inner), p3(newTam,inner), p4(newTam,inner),
            p5(newTam,inner), p6(newTam,inner), p7(newTam,inner),
            aResult(newTam,inner), bResult(newTam,inner);

        int i, j;

        //dividing the matrices in 4 sub-matrices:
        for (i = 0; i < newTam; i++) {
            for (j = 0; j < newTam; j++) {
                a11[i][j] = A[i][j];
                a12[i][j] = A[i][j + newTam];
                a21[i][j] = A[i + newTam][j];
                a22[i][j] = A[i + newTam][j + newTam];

                b11[i][j] = B[i][j];
                b12[i][j] = B[i][j + newTam];
                b21[i][j] = B[i + newTam][j];
                b22[i][j] = B[i + newTam][j + newTam];
            }
        }

        // Calculating p1 to p7:

        sum(a11, a22, aResult, newTam); // a11 + a22
        sum(b11, b22, bResult, newTam); // b11 + b22
        strassenR(aResult, bResult, p1, newTam); // p1 = (a11+a22) * (b11+b22)

        sum(a21, a22, aResult, newTam); // a21 + a22
        strassenR(aResult, b11, p2, newTam); // p2 = (a21+a22) * (b11)

        subtract(b12, b22, bResult, newTam); // b12 - b22
        strassenR(a11, bResult, p3, newTam); // p3 = (a11) * (b12 - b22)

        subtract(b21, b11, bResult, newTam); // b21 - b11
        strassenR(a22, bResult, p4, newTam); // p4 = (a22) * (b21 - b11)

        sum(a11, a12, aResult, newTam); // a11 + a12
        strassenR(aResult, b22, p5, newTam); // p5 = (a11+a12) * (b22)

        subtract(a21, a11, aResult, newTam); // a21 - a11
        sum(b11, b12, bResult, newTam); // b11 + b12
        strassenR(aResult, bResult, p6, newTam); // p6 = (a21-a11) * (b11+b12)

        subtract(a12, a22, aResult, newTam); // a12 - a22
        sum(b21, b22, bResult, newTam); // b21 + b22
        strassenR(aResult, bResult, p7, newTam); // p7 = (a12-a22) * (b21+b22)

        // calculating c21, c21, c11 e c22:

        sum(p3, p5, c12, newTam); // c12 = p3 + p5
        sum(p2, p4, c21, newTam); // c21 = p2 + p4

        sum(p1, p4, aResult, newTam); // p1 + p4
        sum(aResult, p7, bResult, newTam); // p1 + p4 + p7
        subtract(bResult, p5, c11, newTam); // c11 = p1 + p4 - p5 + p7

        sum(p1, p3, aResult, newTam); // p1 + p3
        sum(aResult, p6, bResult, newTam); // p1 + p3 + p6
        subtract(bResult, p2, c22, newTam); // c22 = p1 + p3 - p2 + p6

        // Grouping the results obtained in a single matrix:
        for (i = 0; i < newTam ; i++) {
            for (j = 0 ; j < newTam ; j++) {
                C[i][j] = c11[i][j];
                C[i][j + newTam] = c12[i][j];
                C[i + newTam][j] = c21[i][j];
                C[i + newTam][j + newTam] = c22[i][j];
            }
        }
    }
}

unsigned int nextPowerOfTwo(int n) {
    return pow(2, int(ceil(log2(n))));
}

void strassen(std::vector< std::vector<int> > &A,
              std::vector< std::vector<int> > &B,
              std::vector< std::vector<int> > &C, unsigned int n, double& temps) {
    //unsigned int n = tam;
    unsigned int m = nextPowerOfTwo(n);
    std::vector<int> inner(m);
    std::vector< std::vector<int> > APrep(m, inner), BPrep(m, inner), CPrep(m, inner);

    for(unsigned int i=0; i<n; i++) {
        for (unsigned int j=0; j<n; j++) {
            APrep[i][j] = A[i][j];
            BPrep[i][j] = B[i][j];
        }
    }
    struct timeval t1, t2;
    gettimeofday(&t1, NULL);

    strassenR(APrep, BPrep, CPrep, m);
    for(unsigned int i=0; i<n; i++) {
        for (unsigned int j=0; j<n; j++) {
            C[i][j] = CPrep[i][j];
        }
    }

    gettimeofday(&t2, NULL);
    temps = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
    temps += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
}

// imprime la matrice
void printMat(std::vector<std::vector<int> > mat)
{
    for (unsigned int i = 0; i < mat.size(); ++i)
    {
        for (unsigned int j = 0; j < mat[i].size(); ++j)
        {
            std::cout << mat[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}


// lecture des fichier
std::vector<std::vector<int> > readFile(std::string name)
{
    std::vector<std::vector<int> > mat;
    std::ifstream lec;
    lec.open(name.c_str());

    if (!lec)
       std::cout << "Erreur de lecture \n";

    int taille;
    lec >> taille;

    taille = pow(2.0, taille);
    mat.resize(taille);
    for (int a = 0; a < taille; ++a)
        mat[a].resize(taille);

    for (int i = 0; i < taille; ++i)
        for (int j = 0; j < taille; ++j)
            lec >> mat[i][j];

    return mat;
}

int main(int argc, char *argv[])
{
    std::string myFile1, myFile2 = "";
    leafsize = 256;

    bool resultMult = false;
    int i = 1;
    while (i < argc)
    {
        if ((std::string)argv[i] == "-f")
        {
            myFile1 = argv[i + 1];
            myFile2 = argv[i + 2];
            i += 3;
        }
        else if ((std::string)argv[i] == "-p")
        {
            resultMult = true;
            i += 1;
        }
        else
        {
            break;
        }
    }

    if (!myFile1.empty() && !myFile2.empty())
    {
        std::vector<std::vector<int> > matA = readFile(myFile1);
        std::vector<std::vector<int> > matB = readFile(myFile2);

        double temps;
        std::vector<std::vector<int> > result (matA[0].size(), std::vector<int>(matA[0].size()));
        strassen(matA, matB, result, matA.size(), temps);

//        // Impression dans un fichier des résultats
//        std::ofstream sortie;
//        sortie.open("/usagers/judao/Bureau/INF4705/inf4705/TP1/resultat.txt", std::ios_base::app);
//        sortie << myFile1 << "," << myFile2 << "," << temps << "\n";

        if (resultMult)
        {
            //TODO : afficher le resultat de la multiplication
            printMat(result);
        }
        else
        {
            //TODO : afficher le temps d'execution
            std::cout << "Le temps d'execution est : " << temps << " millisecondes" << std::endl;
        }
    }
    else
        std::cout << "Not enough or invalid arguments.\n";

    return 0;
}
