#include <vector>
#include <iostream>
#include <fstream>
#include <math.h>
#include <sys/time.h>

// imprime la matrice
void printMat(std::vector<std::vector<int> > mat)
{
    for (unsigned int i = 0; i < mat.size(); ++i)
    {
        for (unsigned int j = 0; j < mat[i].size(); ++j)
        {
            std::cout << mat[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}


// multiplication conventionnelle
std::vector<std::vector<int> > mulConv(std::vector<std::vector<int> > matA, std::vector<std::vector<int> > matB, double& temps)
{
    std::vector<std::vector<int> > matResult (matA[0].size(), std::vector<int>(matA[0].size()));
    int val = 0;

    struct timeval t1, t2;

    gettimeofday(&t1, NULL);
    for (unsigned int row = 0; row < matA.size(); ++row)
    {
        for (unsigned int col = 0; col < matA[0].size(); ++col)
        {
            for ( unsigned int k = 0; k < matA[0].size(); ++k)
            {
               val += matA[row][k]*matB[k][col];
            }
            matResult[row][col] = val;
            val = 0 ;
        }
    }
    gettimeofday(&t2, NULL);

    temps = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
    temps += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms

    return matResult;
}

// lecture des fichier
std::vector<std::vector<int> > readFile(std::string name)
{
    std::vector<std::vector<int> > mat;
    std::ifstream lec;
    lec.open(name.c_str());

    if (!lec)
       std::cout << "Erreur de lecture \n";

    int taille;
    lec >> taille;

    taille = pow(2.0, taille);
    mat.resize(taille);
    for (int a = 0; a < taille; ++a)
        mat[a].resize(taille);

    for (int i = 0; i < taille; ++i)
        for (int j = 0; j < taille; ++j)
            lec >> mat[i][j];

    return mat;
}

int main(int argc, char *argv[])
{
    std::string myFile1, myFile2 = "";

	bool resultMult = false;
    int i = 1;
	while (i < argc)
    {
		if ((std::string)argv[i] == "-f")
		{
			myFile1 = argv[i + 1];
			myFile2 = argv[i + 2];
			i += 3;
		}
		else if ((std::string)argv[i] == "-p")
		{
			resultMult = true;
			i += 1;
		}
		else
		{	
			break;
		}
	}

	if (!myFile1.empty() && !myFile2.empty())
	{
		std::vector<std::vector<int> > matA = readFile(myFile1);
		std::vector<std::vector<int> > matB = readFile(myFile2);

        double temps;
        std::vector<std::vector<int> > result = mulConv(matA, matB, temps);

//        // Impression dans un fichier des résultats
//        std::ofstream sortie;
//        sortie.open("/usagers/judao/Bureau/INF4705/inf4705/TP1/resultat.txt", std::ios_base::app);
//        sortie << myFile1 << "," << myFile2 << "," << temps << "\n";

		if (resultMult)
		{
			//TODO : afficher le resultat de la multiplication
            printMat(result);
		}
		else
		{
			//TODO : afficher le temps d'execution
            std::cout << "Le temps d'execution est : " << temps << " millisecondes" << std::endl;
		}
	}
	else
		std::cout << "Not enough or invalid arguments.\n";
        
    return 0;
}
