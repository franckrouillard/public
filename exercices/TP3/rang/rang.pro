#-------------------------------------------------
#
# Project created by QtCreator 2016-03-31T13:44:56
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = rang
CONFIG   += console
CONFIG   -= app_bundle
CONFIG += c++11

TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++11

SOURCES += \
    rang.cpp
