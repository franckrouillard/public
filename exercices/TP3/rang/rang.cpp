#include <vector>
#include <list>
#include <iostream>
#include <fstream>
#include <math.h>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <numeric>
#include <algorithm>

// http://www.geeksforgeeks.org/backtracking-set-7-hamiltonian-cycle/
typedef std::vector<std::vector<int> > mat;

mat matHamil;
std::vector<int> vecTaille;
std::vector<int> solOpt;
int solEtud;

// lecture des fichier
void readFile(std::string name)
{
    std::ifstream lec;
    lec.open(name.c_str());

    if (!lec)
    {
       std::cout << "Erreur de lecture \n";
       exit(0);
    }

    int nbEtudiant;
    lec >> nbEtudiant;

    int nbPair;
    lec >> nbPair;

    matHamil.resize(nbEtudiant);
    vecTaille.resize(nbEtudiant);

    // remplir les tailles
    for (int i = 0; i < nbEtudiant; ++i)
        lec >> vecTaille[i];

    // remplit matrice symetrique
    int x;
    int y;
    for (int i = 0; i < nbPair; ++i)
    {
        lec >> x;
        lec >> y;
        matHamil[x - 1].push_back(y - 1);
        matHamil[y - 1].push_back(x - 1);
    }
}

void afficherSol(std::vector<int> sol)
{
    std::cout << "La meilleure solution est :" << std::endl;
    for (unsigned int i = 0; i < vecTaille.size(); ++i)
        std::cout << solOpt[i] + 1 << "   grandeur: " << vecTaille[solOpt[i]] <<std::endl;
    std::cout << "fin" << std::endl;
}

bool valideVoisin(std::vector<int> solAct, int voisin)
{

    // verifie si noeud pas inclu deja
    for (unsigned int i = 0; i < solAct.size(); ++i)
    {
        if (solAct[i] == voisin)
            return false;
    }

    return true;
}

bool valideSolution(std::vector<int> solAct)
{
    int vision = 1;
    int tailleMax = vecTaille[solAct[0]];

    for (unsigned int i = 1; i < solAct.size(); ++i)
    {
        if (vecTaille[solAct[i]] >= tailleMax)
        {
            tailleMax = vecTaille[solAct[i]];
            ++vision;
        }
    }

    if (vision > solEtud)
    {
        solOpt = solAct;
        solEtud = vision;
        afficherSol(solOpt);
        return true;
    }
    return false;
}

void optimiserSol(std::vector<int> &sol)
{
    std::vector<int> reverseSOl = sol;
    std::reverse(reverseSOl.begin(),reverseSOl.end());

    if(valideSolution(reverseSOl))
    {
        solOpt = reverseSOl;
    }
}

bool hamilRec(std::vector<int>& solAct, struct timeval temps)
{
    struct timeval t2;
    gettimeofday(&t2, NULL);

    if (t2.tv_sec - temps.tv_sec > 5.0)
        return false;

    // si notre chemin est complet
    if (solAct.size() == vecTaille.size())
    {
        valideSolution(solAct);
        return false;
    }

    int noeudCour = solAct[solAct.size()-1];
    // trouver des voisins
    std::vector<int> voisins(matHamil[noeudCour].size());
    std::iota (std::begin(voisins), std::end(voisins), 0);

    int pos;
    for (unsigned int i = voisins.size() - 1; i > 0; --i)
    {
        pos = rand() %i;
        std::swap(voisins[i], voisins[pos]);
    }


    for (unsigned int i = 0; i < voisins.size(); ++i)
    {
        if (valideVoisin(solAct, matHamil[noeudCour][voisins[i]]))
        {
            solAct.push_back(matHamil[noeudCour][voisins[i]]);
            // condition pour retour accelere
            if(!hamilRec(solAct, temps))
                return false;
        }
    }

    // si on n'a pas de voisin
    solAct.pop_back();
    return true;
}

void cheminHamil(double temps)
{
   std::vector<int> solAct;
   solEtud = 0;
   struct timeval t1;

   // pour chaque racine possible
   while (1)
   {
       gettimeofday(&t1, NULL);
       std::cout << "Je suis rendu au root" << std::endl;
       solAct.push_back(rand()% vecTaille.size());
       hamilRec(solAct, t1);
       if (solAct.size() == vecTaille.size())
       {
           //optimisation de la solution actuelle
           optimiserSol(solAct);
           break;
       }

       solAct.clear();
   }
}

int main(int argc, char *argv[])
{
    std::string myFile1 = "";
    bool printResult = false;
    int i = 1;
    srand (time(NULL));

    while (i < argc)
    {
        if ((std::string)argv[i] == "-f")
        {
            myFile1 = argv[i + 1];
            i += 2;
        }
        else if ((std::string)argv[i] == "-p")
        {
            printResult = true;
            i += 1;
        }
        else
        {
            break;
        }
    }

    if (!myFile1.empty())
    {
        readFile(myFile1);

        double temps;
        cheminHamil(temps);

        if (printResult)
        {
            std::cout << "La meilleure solution est :" << std::endl;
            for (unsigned int i = 0; i < vecTaille.size(); ++i)
                std::cout << solOpt[i] + 1<< std::endl;
            std::cout << "fin" << std::endl;
            std::cout << "Le nombre d'etudiant avec la vision obstruee est :" << vecTaille.size() - solEtud << std::endl;

        }
        else
        {
            std::cout << "Le nombre d'etudiant avec la vision obstruee est :" << vecTaille.size() - solEtud << std::endl;
        }


    }
    else
        std::cout << "Not enough or invalid arguments.\n";



    return 0;
}
