#-------------------------------------------------
#
# Project created by QtCreator 2016-03-17T11:22:03
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = vorace
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += \
    vorace.cpp
