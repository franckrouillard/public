#include <vector>
#include <list>
#include <iostream>
#include <fstream>
#include <math.h>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <algorithm>

struct WA {
    int taille;
    int** resto;
    int capacite;
};

// lecture des fichier
struct WA readFile(std::string name)
{
    std::ifstream lec;
    lec.open(name.c_str());

    if (!lec)
    {
       std::cout << "Erreur de lecture \n";
       exit(0);
    }

    struct WA ville;

    //TODO lecture du fichier
    lec >> ville.taille;

    ville.resto = new int*[ville.taille];
    for(int i = 0; i < ville.taille; ++i)
        ville.resto[i] = new int[2];

    for (int i = 0; i < ville.taille; ++i)
    {
        lec >> ville.resto[i][0];
        lec >> ville.resto[i][1];
        lec >> ville.resto[i][2];
    }

    lec >> ville.capacite;

    return ville;
}

std::vector<int> algoDynamique(struct WA ville,double& temps)
{

    std::vector<std::vector<int> > sol2D;
    std::vector<int> sol;

    int somme = 0;
    int taille = ville.taille;
    int capacite = ville.capacite;

    sol2D.resize(taille);
    for (int i = 0; i < taille; ++i)
        sol2D[i].resize(capacite + 1);

    struct timeval t1, t2;
    gettimeofday(&t1, NULL);


    //On remplit le tableau 2D pour l'algo dynamique
    for(int i = 0 ; i < ville.taille; i++)
    {
        for(int j = 0 ; j < ville.capacite + 1; j++)
        {
            sol2D[i][j] = 0;

            if(i - 1 < 0)
            {
                if(ville.resto[i][2] <= j)
                {
                    sol2D[i][j] = ville.resto[i][1];
                }
            }
            else if(j - ville.resto[i][2] < 0)
            {
                sol2D[i][j] = sol2D[i-1][j];
            }
            else
            {
                sol2D[i][j] = std::max(ville.resto[i][1] + sol2D[i - 1][j - ville.resto[i][2]], sol2D[i-1][j]);
            }
        }
    }

    int c = ville.capacite;

    for (int i = ville.taille - 1 ; i >= 0 ; i--)
    {
        if(i - 1 < 0)
        {
            if(somme + ville.resto[i][2] <= ville.capacite)
            {
                sol.push_back(i+1);
            }
        }
        else if(sol2D[i][c] != sol2D[i-1][c])
        {
            if(somme + ville.resto[i][2] <= ville.capacite)
            {
                somme += ville.resto[i][2];
                sol.push_back(i+1);
                c -= ville.resto[i][2];
            }
        }
    }

    gettimeofday(&t2, NULL);

    temps = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
    temps += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms

    return sol;
}

int main(int argc, char *argv[])
{
    std::string myFile1 = "";
    bool printResult = false;
    int i = 1;

    while (i < argc)
    {
        if ((std::string)argv[i] == "-f")
        {
            myFile1 = argv[i + 1];
            i += 2;
        }
        else if ((std::string)argv[i] == "-p")
        {
            printResult = true;
            i += 1;
        }
        else
        {
            break;
        }
    }

    if (!myFile1.empty())
    {

        struct WA ville = readFile(myFile1);
        double temps;
        std::vector<int> solRetenu = algoDynamique(ville,temps);

        if (printResult)
        {
            std::cout << "Les emplacements choisis sont les suivants : ";
            for (unsigned int l = 0; l < solRetenu.size(); ++l)
            {
                std::cout << solRetenu[l] << " ";
            }
            std::cout << std::endl;
        }
        else
        {
            std::cout << "Le temps d'execution est : " << temps << " millisecondes" << std::endl;
        }

        // Impression dans un fichier des résultats
        float revFinal = 0;
        for (unsigned int m = 0; m < solRetenu.size(); ++m)
        {
            revFinal += ville.resto[(int)solRetenu[m] -1][1];
        }

        std::ofstream sortie;
        sortie.open("/usagers/frroud/INF4705/inf4705/TP2/resultat.txt", std::ios_base::app);
        sortie << myFile1 << "," << temps << "," <<  revFinal << "\n";

        for(int i = 0; i < ville.taille; ++i)
            delete ville.resto[i];

        delete ville.resto;
    }
    else
        std::cout << "Not enough or invalid arguments.\n";

    return 0;
}

