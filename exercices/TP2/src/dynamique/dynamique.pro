#-------------------------------------------------
#
# Project created by QtCreator 2016-03-16T21:00:52
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = dynamique
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

CONFIG += c++11
SOURCES += \
    dynamique.cpp
