#include <vector>
#include <list>
#include <iostream>
#include <fstream>
#include <math.h>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <algorithm>
#include <numeric>

struct WA {
    int taille;
    float** resto;
    int capacite;
    float sommeRent;
};

struct Solution {
    float sommeRevenu = 0;
    std::vector<float> vecResto;
};


// lecture des fichier
struct WA readFile(std::string name)
{
    std::ifstream lec;
    lec.open(name.c_str());

    if (!lec)
    {
       std::cout << "Erreur de lecture \n";
       exit(0);
    }

    struct WA ville;

    //TODO lecture du fichier
    lec >> ville.taille;

    ville.resto = new float*[ville.taille];
    for(int i = 0; i < ville.taille; ++i)
        ville.resto[i] = new float[2];

    ville.sommeRent = 0;
    for (int i = 0; i < ville.taille; ++i)
    {
        lec >> ville.resto[i][0];
        lec >> ville.resto[i][1];
        lec >> ville.resto[i][2];
        ville.resto[i][3] = ville.resto[i][1]/ville.resto[i][2];
        ville.sommeRent += ville.resto[i][3];
    }

    lec >> ville.capacite;

    return ville;
}

std::vector<float> algoVorace(struct WA ville, double& temps)
{

    std::list<float*> listeResto;
    std::vector<struct Solution> vecSol(10);
    float prob;
    float accumulation_prob;

    struct timeval t1, t2;

    gettimeofday(&t1, NULL);

    // effectuer le travail 10 fois
    for (int i = 0; i < 10; ++i)
    {
        // Initialiser le test
        listeResto.clear();
        for (int k = 0; k < ville.taille; ++k)
        {
            listeResto.push_back(ville.resto[k]);
        }

        float capaciteRestante = ville.capacite;
        float sommeRent = ville.sommeRent;

        // Tant qu'il est possible de rajouter dans le sac a dos
        while (!listeResto.empty())
        {
           prob = rand()/(float)RAND_MAX;
           accumulation_prob = 0;

           // On cherche le résultat de la prob
           for (std::list<float*>::iterator it = listeResto.begin(); it != listeResto.end(); it++)
           {
                accumulation_prob += (*it)[3]/sommeRent;
                if (prob < accumulation_prob)
                {
                    vecSol[i].sommeRevenu += (*it)[1];
                    vecSol[i].vecResto.push_back((*it)[0]);
                    capaciteRestante -= (*it)[2];
                    sommeRent -= (*it)[3];
                    listeResto.erase(it);
                    break;
                }
           }

           // On retire les éléments qui ne rentre pas dans le reste
           std::list<float*>::iterator it = listeResto.begin();
           while (it != listeResto.end())
           {
                if ((*it)[2] > capaciteRestante)
                {
                    sommeRent -= (*it)[3];
                    it = listeResto.erase(it);
                }
                else
                {
                    ++it;
                }
           }
        }
    }
    float valMax = 0;
    int index = 0;
    // trouver la meilleur solution
    for (int i = 0; i < 10; ++i)
    {
        if(valMax < vecSol[i].sommeRevenu)
        {
            index = i;
            valMax = vecSol[i].sommeRevenu;
        }
    }

    gettimeofday(&t2, NULL);

    temps = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
    temps += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms

    return vecSol[index].vecResto;
}

std::vector<float> algoHeuristique(struct WA ville, std::vector<float> solVorace, double& temps)
{

    // initialisation des vecteurs pour la recherche
    std::vector<float> vecVoisin;
    std::vector<float> vecSolution(solVorace.size());

    float revenuElement;
    float capaciteElement;
    float revenuVoisin;
    float capaciteVoisin;
    float revenuMax;
    float capaciteMax;

    bool continuer = true;
    std::vector<float> solMax = solVorace;

    std::vector<float> v(ville.taille) ; // vector with 100 ints.
    std::iota (std::begin(v), std::end(v), 1); // Fill with 0, 1, ..., 99.

    struct timeval t1, t2;

    gettimeofday(&t1, NULL);

    // tant que l'on trouve une nouvelle solution dans le voisinage
    while (continuer)
    {
        continuer = false;

        vecVoisin.resize(ville.taille);

        vecSolution = solMax;
        revenuMax = 0;
        capaciteMax = 0;
        std::sort(vecSolution.begin(), vecSolution.end());
        std::vector<float>::iterator it = std::set_difference (v.begin(), v.end(), vecSolution.begin(), vecSolution.end(), vecVoisin.begin());
        vecVoisin.resize(it-vecVoisin.begin());

        for (unsigned int k = 0; k < vecSolution.size(); ++k)
        {
            //vecVoisin.erase(vecVoisin.begin() + vecSolution[k] - 1);
            revenuMax += ville.resto[(int)vecSolution[k] -1][1];
            capaciteMax += ville.resto[(int)vecSolution[k] -1][2];
        }

        // pour chaque element de la solution actuelle (singulier)
        for (unsigned int k = 0; k < vecSolution.size(); ++k)
        {
            revenuElement = ville.resto[(int)vecSolution[k] -1][1];
            capaciteElement = ville.resto[(int)vecSolution[k] -1][2];

            // pour chaque 1er element du voisinage
            for (unsigned int l = 0; l < vecVoisin.size(); ++l)
            {
                revenuVoisin = ville.resto[(int)vecVoisin[l] -1][1];
                capaciteVoisin = ville.resto[(int)vecVoisin[l] - 1][2];

                // Si on peut rajouter le voisin a la solution
                if ((capaciteMax - capaciteElement + capaciteVoisin) <= ville.capacite)
                {
                    // verifie si augmentation du revenu
                    if (revenuMax < revenuMax - revenuElement + revenuVoisin)
                    {
                        revenuMax = revenuMax - revenuElement + revenuVoisin;
                        revenuElement = revenuVoisin;
                        capaciteElement = capaciteVoisin;
                        solMax = vecSolution;
                        solMax[k] = vecVoisin[l];

                        continuer = true;
                    }

                    // On ajoute un 2e element du voisinage
                    for (unsigned int m = l + 1; m < vecVoisin.size(); ++m)
                    {
                        revenuVoisin += ville.resto[(int)vecVoisin[m] -1][1];
                        capaciteVoisin += ville.resto[(int)vecVoisin[m] -1][2];

                        // Si on peut rajouter le voisin a la solution
                        if ((capaciteMax - capaciteElement + capaciteVoisin) <= ville.capacite)
                        {
                            // verifie le changement du premier element
                            if (revenuMax < revenuMax - revenuElement + revenuVoisin)
                            {
                                revenuMax = revenuMax - revenuElement + revenuVoisin;
                                solMax = vecSolution;
                                solMax[k] = vecVoisin[l];
                                solMax.push_back(vecVoisin[m]);

                                continuer = true;
                            }
                        }

                        revenuVoisin = ville.resto[(int)vecVoisin[l] -1][1];
                        capaciteVoisin = ville.resto[(int)vecVoisin[l] -1][2];
                    }

                }
            }
        }

        // pour chaque element de la solution actuelle (multiple)
        for (unsigned int k = 0; k < vecSolution.size(); ++k)
        {
            revenuElement = ville.resto[(int)vecSolution[k] -1][1];
            capaciteElement = ville.resto[(int)vecSolution[k] -1][2];

            // avec chaque 2e element de la solution actuelle
            for (unsigned int l = k + 1; l < vecSolution.size(); ++l)
            {
                revenuElement += ville.resto[(int)vecSolution[l] -1][1];
                capaciteElement += ville.resto[(int)vecSolution[l] -1][2];

                // ajoute 1 element du voisinage
                for (unsigned int n = 0; n < vecVoisin.size(); ++n)
                {
                    revenuVoisin = ville.resto[(int)vecVoisin[n] -1][1];
                    capaciteVoisin = ville.resto[(int)vecVoisin[n] -1][2];

                    // Si on peut rajouter le voisin a la solution
                    if ((capaciteMax - capaciteElement + capaciteVoisin) <= ville.capacite)
                    {
                        // verifie si augmentation du revenu
                        if (revenuMax < revenuMax - revenuElement + revenuVoisin)
                        {
                            revenuMax = revenuMax - revenuElement + revenuVoisin;
                            revenuElement = revenuVoisin;
                            capaciteElement = capaciteVoisin;
                            solMax = vecSolution;
                            solMax[k] = vecVoisin[n];
                            solMax.erase(solMax.begin() + l);
                            continuer = true;
                        }

                        // On ajoute un 2e element du voisinage
                        for (unsigned int m = n + 1; m < vecVoisin.size(); ++m)
                        {
                            revenuVoisin += ville.resto[(int)vecVoisin[m] -1][1];
                            capaciteVoisin += ville.resto[(int)vecVoisin[m] -1][2];

                            // Si on peut rajouter le voisin a la solution
                            if ((capaciteMax - capaciteElement + capaciteVoisin) <= ville.capacite)
                            {
                                // verifie le changement du premier element
                                if (revenuMax < revenuMax - revenuElement + revenuVoisin)
                                {
                                    revenuMax = revenuMax - revenuElement + revenuVoisin;
                                    solMax = vecSolution;
                                    solMax[k] = vecVoisin[n];
                                    solMax[l] = vecVoisin[m];

                                    continuer = true;
                                }
                            }

                            revenuVoisin = ville.resto[(int)vecVoisin[n] -1][1];
                            capaciteVoisin = ville.resto[(int)vecVoisin[n] -1][2];
                        }
                    }
                }

                revenuElement = ville.resto[(int)vecSolution[k] -1][1];
                capaciteElement = ville.resto[(int)vecSolution[k] -1][2];
            }
        }

        // fin multiple
        gettimeofday(&t2, NULL);
        if (t2.tv_sec - t1.tv_sec > 20)
        {
            std::cout << "Trop long \n";
            break;
        }
    }

    gettimeofday(&t2, NULL);

    temps = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
    temps += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms

    // fin while
    return solMax;
}

int main(int argc, char *argv[])
{
    std::string myFile1 = "";
    bool printResult = false;
    int i = 1;
    srand (time(NULL));

    while (i < argc)
    {
        if ((std::string)argv[i] == "-f")
        {
            myFile1 = argv[i + 1];
            i += 2;
        }
        else if ((std::string)argv[i] == "-p")
        {
            printResult = true;
            i += 1;
        }
        else
        {
            break;
        }
    }

    if (!myFile1.empty())
    {
        struct WA ville = readFile(myFile1);
        double temps;
        std::vector<float> solRetenu = algoVorace(ville,temps);

        // TODO algo heuristique
        std::vector<float> solLocale = algoHeuristique(ville,solRetenu,temps);

        if (printResult)
        {
            std::cout << "Les emplacements choisis sont les suivants : ";
            for (unsigned int l = 0; l < solLocale.size(); ++l)
            {
                std::cout << solLocale[l] << " ";
            }
            std::cout << std::endl;
        }
        else
        {
           std::cout << "Le temps d'execution est : " << temps << " millisecondes" << std::endl;
        }

        // Impression dans un fichier des résultats
        float revFinal = 0;
        for (int m = 0; m < solLocale.size(); ++m)
        {
            revFinal += ville.resto[(int)solLocale[m] -1][1];
        }

        std::ofstream sortie;
        sortie.open("/usagers/frroud/INF4705/inf4705/TP2/resultat.txt", std::ios_base::app);
        sortie << myFile1 << "," << temps << "," <<  revFinal << "\n";

        for(int i = 0; i < ville.taille; ++i)
            delete ville.resto[i];

        delete ville.resto;
    }
    else
        std::cout << "Not enough or invalid arguments.\n";

    return 0;
}
