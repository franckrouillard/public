#-------------------------------------------------
#
# Project created by QtCreator 2016-03-16T13:02:22
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = heuristique
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app
CONFIG += c++11
QMAKE_CXXFLAGS += -std=c++11

SOURCES += \
    heuristique.cpp
