#include <vector>
#include <list>
#include <iostream>
#include <fstream>
#include <math.h>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>

struct WA {
    int taille;
    float** resto;
    int capacite;
    float sommeRent;
};

struct Solution {
    float sommeRevenu = 0;
    std::vector<float> vecResto;
};


// lecture des fichier
struct WA readFile(std::string name)
{
    std::ifstream lec;
    lec.open(name.c_str());

    if (!lec)
       std::cout << "Erreur de lecture \n";

    struct WA ville;

    //TODO lecture du fichier
    lec >> ville.taille;

    ville.resto = new float*[ville.taille];
    for(int i = 0; i < ville.taille; ++i)
        ville.resto[i] = new float[2];

    ville.sommeRent = 0;
    for (int i = 0; i < ville.taille; ++i)
    {
        lec >> ville.resto[i][0];
        lec >> ville.resto[i][1];
        lec >> ville.resto[i][2];
        ville.resto[i][3] = ville.resto[i][1]/ville.resto[i][2];
        ville.sommeRent += ville.resto[i][3];
    }

    lec >> ville.capacite;

    return ville;
}

std::vector<float> algoVorace(struct WA ville,double& temps)
{

    std::list<float*> listeResto;
    std::vector<struct Solution> vecSol(10);
    float prob;
    float accumulation_prob;

    struct timeval t1, t2;

    gettimeofday(&t1, NULL);

    // effectuer le travail 10 fois
    for (int i = 0; i < 10; ++i)
    {
        // Initialiser le test
        listeResto.clear();
        for (int k = 0; k < ville.taille; ++k)
        {
            listeResto.push_back(ville.resto[k]);
        }

        float capaciteRestante = ville.capacite;
        float sommeRent = ville.sommeRent;

        // Tant qu'il est possible de rajouter dans le sac a dos
        while (!listeResto.empty())
        {
           prob = rand()/(float)RAND_MAX;
           accumulation_prob = 0;

           // On cherche le résultat de la prob
           for (std::list<float*>::iterator it = listeResto.begin(); it != listeResto.end(); it++)
           {
                accumulation_prob += (*it)[3]/sommeRent;
                if (prob < accumulation_prob)
                {
                    vecSol[i].sommeRevenu += (*it)[1];
                    vecSol[i].vecResto.push_back((*it)[0]);
                    capaciteRestante -= (*it)[2];
                    sommeRent -= (*it)[3];
                    listeResto.erase(it);
                    break;
                }
           }

           // On retire les éléments qui ne rentre pas dans le reste
           std::list<float*>::iterator it = listeResto.begin();
           while (it != listeResto.end())
           {
                if ((*it)[2] > capaciteRestante)
                {
                    sommeRent -= (*it)[3];
                    it = listeResto.erase(it);
                }
                else
                {
                    ++it;
                }
           }
        }
    }
    float valMax = 0;
    int index = 0;
    // trouver la meilleur solution
    for (int i = 0; i < 10; ++i)
    {
        if(valMax < vecSol[i].sommeRevenu)
        {
            index = i;
            valMax = vecSol[i].sommeRevenu;
        }
    }

    gettimeofday(&t2, NULL);

    temps = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
    temps += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms


    return vecSol[index].vecResto;
}

int main(int argc, char *argv[])
{
    std::string myFile1 = "";
    bool printResult = false;
    int i = 1;
    srand (time(NULL));

    while (i < argc)
    {
        if ((std::string)argv[i] == "-f")
        {
            myFile1 = argv[i + 1];
            i += 2;
        }
        else if ((std::string)argv[i] == "-p")
        {
            printResult = true;
            i += 1;
        }
        else
        {
            break;
        }
    }

    if (!myFile1.empty())
    {
        struct WA ville = readFile(myFile1);
        double temps;
        std::vector<float> solRetenu = algoVorace(ville,temps);

        if (printResult)
        {
            std::cout << "Les emplacements choisis sont les suivants : ";
            for (unsigned int l = 0; l < solRetenu.size(); ++l)
            {
                std::cout << solRetenu[l] << " ";
            }
            std::cout << std::endl;
        }
        else
        {
           std::cout << "Le temps d'execution est : " << temps << " millisecondes" << std::endl;
        }

        // Impression dans un fichier des résultats
        float revFinal = 0;
        for (int m = 0; m < solRetenu.size(); ++m)
        {
            revFinal += ville.resto[(int)solRetenu[m] -1][1];
        }

        std::ofstream sortie;
        sortie.open("/usagers/frroud/INF4705/inf4705/TP2/resultat.txt", std::ios_base::app);
        sortie << myFile1 << "," << temps << "," <<  revFinal << "\n";


        for(int i = 0; i < ville.taille; ++i)
            delete ville.resto[i];

        delete ville.resto;

    }
    else
        std::cout << "Not enough or invalid arguments.\n";



    return 0;
}
