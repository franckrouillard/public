#-------------------------------------------------
#
# Project created by QtCreator 2016-03-11T12:26:42
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = TP2
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

CONFIG += c++11


SOURCES += \
    vorace.cpp
